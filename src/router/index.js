import Vue from "vue";
import VueRouter from "vue-router";

import TeamsList from "@/components/teams/TeamsList";
import TheHome from "@/components/nav/TheHome";
import UsersList from "@/components/users/UsersList";
import TeamMembers from "@/components/teams/TeamMembers";
import DataServices from "@/components/services/DataServices";
import DigitalServices from "@/components/services/DigitalServices";
import MarketingServices from "@/components/services/MarketingServices";
import TeamsFooter from "@/components/nav/TeamsFooter";
import TheForm from "@/components/form/TheForm";

Vue.use(VueRouter);

const routes = [{
            path: "/teams",
            component: TeamsList,
        },

        {
            path: "/nav",
            components: {
                default: TheHome,
                footer: TeamsFooter,
            },
        },
        {
            path: "/users",
            components: {
                default: UsersList,
                footer: TeamsFooter,
            },
        },
        {
            path: "/teams/:teamId",
            components: {
                default: TeamMembers,
                footer: TeamsFooter,
            },
        },

        {
            path: "/",
            redirect: "/nav",
        },
        {
            path: "/data",
            component: DataServices,
        },
        {
            path: "/digital",
            components: {
                default: DigitalServices,
                footer: TeamsFooter,
            },
        },
        {
            path: "/marketing",
            component: MarketingServices,
        },
        {
            path: "/form",
            components: { default: TheForm, footer: TeamsFooter },
        },
    ],
    router = new VueRouter({
        mode: "history",
        base: process.env.BASE_URL,
        routes,
    });

export default router;